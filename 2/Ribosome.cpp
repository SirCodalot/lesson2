#include "Ribosome.h"
#include <iostream>

Protein* Ribosome::create_protein(std::string& RNA_transcript) const
{
	Protein* protein = new Protein;
	protein->init();
	for (int i = 0; i < RNA_transcript.size(); i += 3)
	{
		if (RNA_transcript.size() < i + 3)
			break;

		std::string noks = "";
		noks += RNA_transcript.at(i);
		noks += RNA_transcript.at(i + 1);
		noks += RNA_transcript.at(i + 2);

		AminoAcid acid = get_amino_acid(noks);
		if (acid == UNKNOWN)
		{	
			protein->clear();
			delete(protein);
			return nullptr;
		}

		protein->add(acid);

	}

	return protein;
}
