#pragma once
#include "Protein.h"
#include "AminoAcid.h"
#include <iostream>

class Ribosome
{
public:
	Protein * create_protein(std::string &RNA_transcript) const;
};