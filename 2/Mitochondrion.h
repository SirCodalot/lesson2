#pragma once
#include "Protein.h"
#include "AminoAcid.h"
#include <iostream>

class Mitochondrion
{
public:
	void init();

	void insert_glucose_receptor(const Protein & protein);
	void set_glucose(const unsigned int glucose_units);
	
	bool produceATP() const;

private:
	unsigned int _glocuse_level;
	bool _has_glocuse_receptor;
};