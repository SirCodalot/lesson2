#include "Mitochondrion.h"

void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	AminoAcid* acids = new AminoAcid[7]{ALANINE, LEUCINE, GLYCINE, HISTIDINE, LEUCINE, PHENYLALANINE, AMINO_CHAIN_END};
	AminoAcidNode* acid = protein.get_first();

	for (int i = 0; i < 7 && acid; i++)
	{
		std::cout << i;
		if (acids[i] != acid->get_data())
			return;
		acid = acid->get_next();
	}

	this->_has_glocuse_receptor = true;
}

void Mitochondrion::set_glucose(const unsigned glucose_units)
{
	this->_glocuse_level = glucose_units;
}

bool Mitochondrion::produceATP() const
{
	return this->_has_glocuse_receptor && this->_glocuse_level >= 50;
}
