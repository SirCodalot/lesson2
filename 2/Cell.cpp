#include "Cell.h"
#include <iostream>

void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_mitochondrion.init();

	this->_glocuse_receptor_gene = glucose_receptor_gene;
}

bool Cell::get_ATP()
{
	std::string transcript = this->_nucleus.get_RNA_transcript(this->_glocuse_receptor_gene);
	Protein* protein = this->_ribosome.create_protein(transcript);
	
	if (!protein)
	{
		std::cerr << "Cannot Make Protein";
		_exit(1);
		return false;
	}

	this->_mitochondrion.insert_glucose_receptor(*protein);
	bool success = this->_mitochondrion.produceATP();

	if (!success)
		return false;

	this->_atp_units = 100;
	return true;
}
