#include "Nucleus.h"
#include <iostream>
#include "AminoAcid.h"

// Gene

void Gene::init(const unsigned start, const unsigned end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

unsigned Gene::get_start() const
{
	return this->_start;
}

unsigned Gene::get_end() const
{
	return this->_end;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}

void Gene::set_start(unsigned _start)
{
	this->_start = _start;
}

void Gene::set_end(unsigned _end)
{
	this->_end = _end;
}

void Gene::set_on_complementary_dna_strand(unsigned _on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = _on_complementary_dna_strand;
}

// Nucleus

void Nucleus::init(const std::string dna_sequence)
{
	this->_DNA_strand = dna_sequence;
	this->_complementary_DNA_strand = "";

	for (int i = 0; i < dna_sequence.size(); i++)
	{
		switch (dna_sequence.at(i))
		{
			case 'G':
				this->_complementary_DNA_strand += "C";
				break;
			case 'C':
				this->_complementary_DNA_strand += "G";
				break;
			case 'T':
				this->_complementary_DNA_strand += "A";
				break;
			case 'A':
				this->_complementary_DNA_strand += "T";
				break;
			default:
				std::cerr << "Invalid DNA" << std::endl;
				_exit(1);
				return;
		}
	}
}

std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	std::string strand = gene.is_on_complementary_dna_strand() ? this->_complementary_DNA_strand : this->_DNA_strand;
	std::string transcript = "";

	for (int i = gene.get_start(); i < gene.get_end(); i++)
	{
		if (gene.is_on_complementary_dna_strand() && strand.at(i) == 'T')
		{
			transcript += "U";
		}
		else
		{
			transcript += strand.at(i);
		}
	}

	return transcript;
}

std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string reversed = this->_DNA_strand;
	int size = reversed.size();
	for (int i = 0; i < size; i++)
	{
		std::swap(reversed[i], reversed[size - i - 1]);
	}
	return reversed;
}

unsigned Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	unsigned count = 0;
	std::string::size_type pos = 0;
	while((pos = this->_DNA_strand.find(codon, pos)) != std::string::npos)
	{
		count++;
		pos += codon.size();
	}
	return count;
}


